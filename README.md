# Web Crawler for scraping Financial data  


## Working:

 This approach involves the following steps:
 1. Logging into a data provider server using selenium web driver.
 2. Submitting the query that filters the stocks qualifying our criteria(used Piotroski score here)
 3. Collecting the stocks links storing in the list from the first page (if results are more than a page).
 4. Looping over stock link page and fetching required data, and simultaneously generating Plots
    and xlsx files for every link(stock).
 5. Move to the next page and repeat steps 3-4 until it hits the last page(Or any number or pages you specify).


## Requirements:
1. Python
2. Selenium
3. Beautifulsoup
4. Numpy


## Usage
###  Login 

Create an account on and add in these credentials inside the program
https://www.screener.in/login/

Enter this username and password inside the scraper.

**Make sure you dont commit your username or password inside the code**

### Iterating search results
After you have created an account, run the app by typing: python scraper.py -u username -p password

Initially we will give a query inside screener.in, here I have given a set of filters in screen.in as a list of urls inside the progra,, it can be anything based on your requirement. You can add more filters as per your requirement

Where i corresponds to the page number of the search result. All pages will be looped through by default. You can 
manually limit it to a certain number of pages by altering the range of this loop

note: These only give potentially good stocks that pass through the filters that we give, further analysis of these stocks need to be done before making a buy/sell



Key insights being scraped:

1. Sales growth
2. Operating profit Margin
3. Net profit growth
4. Asset Purchased 
5. Reinvestment into Business
6. Return on Capital employed 
7. Free cash flows 
8. Debt
9. EPS

